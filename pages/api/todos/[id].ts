
import { NextApiHandler, NextApiRequest, NextApiResponse } from "next"
import { findById, removeTodo, saveTodo } from "../../../lib/todo-repository"

const updateTodoHandler: NextApiHandler = async (req, res) => {
  const {query: {id}} = req
  const found = await findById(
    Number.parseInt(id as string, 10)
  )
  if (found === null) {
    res.status(404).end()
  } else {
    const updated = {...found, title: req.body.title, done: req.body.done}
    await saveTodo(updated)
    res.send(updated)
  }
}

const removeTodoHandler: NextApiHandler = async (req, res) => {
  const {query: {id}} = req
  const found = await findById(
    Number.parseInt(id as string, 10)
  )
  if (found === null) {
    res.status(404).end()
  } else {
    await removeTodo(found)
    res.status(200).end()
  }
}

export default function (req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case "PUT":
      return updateTodoHandler(req, res)
    case "DELETE":
      return removeTodoHandler(req, res)
    default:
      res.status(405).end()
      return
  }
}