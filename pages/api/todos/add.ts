import { NextApiResponse, NextApiRequest } from "next"
import { createTodo } from "../../../lib/todo-repository"

export default async function (req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const newTodo = await createTodo(req.body.title)
    res.status(201).send(newTodo)
  } else {
    res.status(405).end()
  }
}