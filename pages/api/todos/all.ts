import { NextApiRequest, NextApiResponse } from "next";
import { allActive, allDone, allTodos } from "../../../lib/todo-repository";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "GET") {
    const {
      query: { status },
    } = req
    switch (status) {
      case "done":
        res.send(await allDone())
        break;
      case "active":
        res.send(await allActive())
        break;
      default:
        res.send(await allTodos())
        break;
    } 
  } else {
    res.status(405).end()
  }
}