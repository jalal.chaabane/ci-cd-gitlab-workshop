import React, {useState} from "react"
import { Container, List, ListItem, ListItemIcon, ListItemSecondaryAction, Checkbox, ListItemText, IconButton, Typography, Card, TextField } from '@material-ui/core'
import { DeleteOutlineOutlined } from "@material-ui/icons"
import axios from "axios"
import { Todo } from "@prisma/client"
import { allTodos } from "../lib/todo-repository"
import * as Sentry from "@sentry/node"

type SetState<A> = (f: A | ((a: A) => A)) => void

const addTodo = async (title: string, setNewTitle: SetState<string>, setTodos: SetState<Todo[]>) => {
  try {
    const response = await axios.post("/api/todos/add", {title})
    const newTodo = response.data
    setTodos(todos => [...todos, newTodo])
    setNewTitle("")
  } catch (err) {
    alert("Une erreur est survenu lors de l'ajout d'une tâche")
  }
}

const removeTodo = async (todoToRemove: Todo, setTodos: SetState<Todo[]>) => {
  try {
    await axios.delete(`/api/todos/${todoToRemove.id}`)
    setTodos(todos => todos.filter(todo => todo.id !== todoToRemove.id))
  } catch (err) {
    alert("Une erreur est survenu lors de l'ajout d'une tâche")
  }
}

const toggleDone = async (todo: Todo, setTodos: SetState<Todo[]>) => {
  try {
    console.log("send exception to sentry")
    Sentry.captureException(new Error("DONE"))
    const updatedTodo = {...todo, done: !todo.done}
    await axios.put(`/api/todos/${todo.id}`, updatedTodo)
    setTodos(todos => 
      todos.map(
        todo => todo.id === updatedTodo.id ? updatedTodo : todo
      )
    )
  } catch (err) {
    alert("Une erreur est survenu lors de l'ajout d'une tâche")
  }
}

interface Props {
  todos: Todo[]
}

export default function Home(props: Props) {
  const [todos, setTodos] = useState(props.todos)
  const [newTaskTitle, setNewTaskTitle] = useState("")

  return (
    <Container maxWidth="sm">
      <Typography variant="h3" component="h3" align="center" style={{padding: 50}}>
        Tasks
      </Typography>
      <Card>
        <List>
          <ListItem dense>
            <ListItemText>
              <TextField
                value={newTaskTitle}
                onKeyUp={evt => {
                  if (evt.code === "Enter") {
                    addTodo(newTaskTitle, setNewTaskTitle, setTodos)
                  }
                }}
                fullWidth 
                placeholder="Nouvelle tâche" 
                onChange={evt => setNewTaskTitle(evt.target.value)} />
            </ListItemText>
          </ListItem>
          {todos.map(todo => 
            <ListItem key={todo.id} dense button onClick={console.log}>
              <ListItemIcon>
                <Checkbox 
                  edge="start"
                  checked={todo.done}
                  onChange={() => toggleDone(todo, setTodos)}
                  disableRipple
                />
              </ListItemIcon>
              <ListItemText primary={todo.title} />
              <ListItemSecondaryAction>
                <IconButton edge="end" onClick={() => removeTodo(todo, setTodos)}>
                  <DeleteOutlineOutlined />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          )}
        </List>
      </Card>
    </Container>
  )
}

export async function getServerSideProps() {
  return {
    props: {
      todos: await allTodos()
    }
  }
}