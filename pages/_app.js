import React from "react"
import '../styles/globals.css'
import 'fontsource-roboto';
import * as Sentry from '@sentry/node';

if (process.env.NEXT_PUBLIC_SENTRY_DSN) {
  Sentry.init({
    enabled: process.env.NODE_ENV === 'production',
    dsn: process.env.NEXT_PUBLIC_SENTRY_DSN
  });
}

function MyApp({ Component, pageProps, err }) {
  return <Component {...pageProps} err={err} />
}

export default MyApp
