import { Todo } from "@prisma/client";
import { prisma } from "./db";

export const createTodo = (title: string): Promise<Todo> => {
  return prisma.todo.create({
    data: {
      title,
      done: false
    }
  })
}

export const saveTodo = (todo: Todo): Promise<Todo> => {
  return prisma.todo.update({
    data: {
      done: todo.done,
      title: todo.title
    },
    where: {
      id: todo.id
    }
  })
}

export const removeTodo = async (todo: Todo): Promise<void> => {
  await prisma.todo.delete({
    where: {
      id: todo.id
    }
  })
}

export const allTodos = (): Promise<Todo[]> => prisma.todo.findMany()

export const allActive = (): Promise<Todo[]> => prisma.todo.findMany({
  where: {
    done: {
      equals: false
    }
  }
})

export const allDone = (): Promise<Todo[]> => prisma.todo.findMany({
  where: {
    done: {
      equals: true
    }
  }
})

export const findById = (id: number): Promise<Todo | null> =>
  prisma.todo.findUnique({
    where: {
      id: id
    }
  })