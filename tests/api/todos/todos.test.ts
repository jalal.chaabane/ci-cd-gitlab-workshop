import { range } from "lodash"
import { createMocks, MockResponse } from "node-mocks-http"
import { prisma } from "../../../lib/db"
import allTodoApi from "../../../pages/api/todos/all"
import addTodoApi from "../../../pages/api/todos/add"
import todoApi from "../../../pages/api/todos/[id]"
import faker from "faker"
import { Todo } from "@prisma/client"
import { allTodos } from "../../../lib/todo-repository"

const seedDb = async () => {
  const seeds = range(0, 20).map(() => {
    return () => {
      return prisma.todo.create({
        data: {
          title: faker.name.title(),
          done: faker.random.boolean()
        }
      })
    }
  })
  for (const seed of seeds) {
    await seed()
  }
}

describe("Test Todos API", () => {

  beforeEach(async () => {
    await seedDb()
  })

  afterEach(async () => {
    await prisma.todo.deleteMany()
  })

  it("should get all todos", async () => {
    const {req, res} = createMocks({
      method: "GET"
    })
    const response = res as MockResponse<Response>
    await allTodoApi(req, res)
    expect(response._getData().length).toBe(20)
  })

  it("should get all active todos", async () => {
    const {req, res} = createMocks({
      method: "GET",
      query: {
        status: "active"
      }
    })
    const response = res as MockResponse<Response>
    await allTodoApi(req, res)
    expect(response._getData().every((todo: Todo) => !todo.done)).toBeTruthy()
  })

  it("should get all done todos", async () => {
    const {req, res} = createMocks({
      method: "GET",
      query: {
        status: "done"
      }
    })
    const response = res as MockResponse<Response>
    await allTodoApi(req, res)
    expect(response._getData().every((todo: Todo) => todo.done)).toBeTruthy()
  })

  it("should add a todo", async () => {
    const {req, res} = createMocks({
      method: "POST",
      body: {
        title: "NewTodo"
      }
    })
    const response = res as MockResponse<Response>
    await addTodoApi(req, res)
    expect(response._getData().title).toBe("NewTodo")
  })

  it("should remove a todo", async () => {
    let todos = await allTodos()
    const todoToRemove = todos[0]
    const {req, res} = createMocks({
      method: "DELETE",
      query: {
        id: todoToRemove.id
      }
    })
    const response = res as MockResponse<Response>
    await todoApi(req, res)
    todos = await allTodos()
    expect(response._getStatusCode()).toBe(200)
    expect(todos.find(todo => todo.id === todoToRemove.id)).toBeUndefined()
  })

  it("should be able to update a todo", async () => {
    let todos = await allTodos()
    const todoToUpdate = todos[0]
    const {req, res} = createMocks({
      method: "PUT",
      query: {
        id: todoToUpdate.id
      },
      body: {
        title: "NewTitle",
        done: true
      }
    })
    const response = res as MockResponse<Response>
    await todoApi(req, res)
    todos = await allTodos()
    expect(response._getStatusCode()).toBe(200)
    const updated = todos.find(todo => todo.id === todoToUpdate.id)
    expect(updated).not.toBeNull()
    expect(updated?.title).toBe("NewTitle")
    expect(updated?.done).toBeTruthy()
  })
})